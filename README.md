# Configure OpenID Connect (OIDC) between GCP and GitLab


## Use-cases
* Retrieve temporary credentials from GCP to access cloud services
* Use credentials to retrieve secrets or deploy to an environment
* Scope role to branch or project

For additional details, see [documentation here](https://cloud.google.com/iam/docs/configuring-workload-identity-federation#oidc)


## Steps


## Resources

- https://cloud.google.com/iam/docs/configuring-workload-identity-federation#oidc
